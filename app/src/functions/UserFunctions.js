import axios from "axios"

export const login = user => {
  return axios
//     .post("http://127.0.0.1:5000/users/login", {
     .post("https://securityapibackend.azurewebsites.net/users/login", {
      email: user.email,
      password: user.password
    })
    .then(response => {
      if(response.data.token !== undefined)
        localStorage.setItem('token',response.data.token);
      return response.data
    })
    .catch(err => {
      console.log(err)
    })
}