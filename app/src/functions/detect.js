export const tablet = 'tablet';
export const mobile = 'mobile';
export const desktop = 'desktop';
export const isTablet = navigator.userAgent.match(/Tablet|iPad/i);
export const isMobile = navigator.userAgent.match(/Android|webOS|iPhone|iPod|Blackberry/i);
export const isDesktop = !isMobile && !isTablet;
export const device = (isMobile? mobile: (isTablet? tablet:desktop));

export const portrait = "portrait";
export const landscape = "landscape";