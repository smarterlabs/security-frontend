import React from 'react';
import ReactDOM from "react-dom"
import {BrowserRouter as Router, Switch, Redirect} from "react-router-dom";
import SignIn from "./signin/container/SignInContainer";
import {MyRoute} from "./functions/RoutePath";
import {PrivateRoute} from "./functions/PrivateRoute";
import InputContainer from "./inputpage/container/InputContainer";
import history from './functions/history';
import './App.css';


async function init() {
    const routing = (
        <Router history={history}>
            <Switch>
                <MyRoute path={"/login"} component={SignIn}/>
                <PrivateRoute path={"/inputs"} component={InputContainer} />
                <Redirect to="/login"/>

            </Switch>
        </Router>
    );
    ReactDOM.render(routing, document.getElementById("root"));
}

init();