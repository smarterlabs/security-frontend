import React from "react";
import {withStyles} from '@material-ui/core/styles';
import {CardHeader, Grid, TextField} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import {getInputData} from "../controller/InputAction";

const styles = theme => ({
    structure: {
        flexGrow: 1,
    },
    inputRoot: {
        fontSize: 17,
        color: "rgba(0,0,0,0.9)"
    },
    menuPaper: {
        maxHeight: 300,
    },
    formControl: {
        margin: 2,
        minWidth: 120,
    },
    select: {
        textAlign: "center",
        fontSize: 20,
    },
    Input: {
        borderStyle: "solid",
        // borderWidth: "1px",
    },
    large: {

        backgroundColor: 'white',
        width: theme.spacing(10),
        height: theme.spacing(10),
    }

});


class DashboardContainer extends React.Component {
    
    componentDidMount() {
        getInputData().then(({data: response}) => {
            if ('redirect' in response){
                this.props.history.push('/login')
            }
            this.setState({
                jsonResponse: response,
            });
          
        });

    }

    handleOpenModal() {
        this.setState({openModal: !this.state.openModal});
    }

    render() {
        const {classes} = this.props;

        return <>
            <div className={classes.structure}>
                <div className="wrapper">
                    <div className="page_header">
                        <div className="page">
                            <Grid container direction="row" justify="center" alignItems="center">
                                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} >
                                    <CardHeader
                                       
                                        titleTypographyProps={{variant: "h4", color: 'textPrimary'}}
                                        title={"Login successful"}
                                    />
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                    <div className="page pageBody">
                        <Grid container  direction="row" justify="center" alignItems="flex-start">
                            <Grid item xs={12}>
                                <Typography className="MB30">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ultrices vitae elit at ultricies. In ornare sit amet quam ut hendrerit. Cras eu egestas ante. In hac habitasse platea dictumst. Sed tempus metus et nunc tincidunt, id dignissim arcu pretium.
                                </Typography>
                            </Grid>
                        </Grid>

                    </div>
                </div>
            </div>
        </>;
    }
}

export default (withStyles(styles)(DashboardContainer));
