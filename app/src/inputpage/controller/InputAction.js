import axios from "axios/index";



export const getInputData = () => {
  return axios
//      .post("http://127.0.0.1:5000/inputs_data", {
      .post("https://securityapibackend.azurewebsites.net/inputs_data", {
          token: localStorage.getItem("token")
      })
      .then(response => {
        return response
      })
      .catch(err => {
        console.log(err)
      })
}

export const dataDashboard = async () => await axios.post('/api/dashboard', {
    token: localStorage.getItem("token")
});