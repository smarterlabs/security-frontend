import React from "react";
import {withStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import {login} from "../../functions/UserFunctions"
import {CardHeader} from "@material-ui/core";

const styles = theme => ({

    inputRoot: {
        fontSize: 20,
        color:"rgba(0,0,0,0.9)"
    },
    input: {
        '&::placeholder': {
            color:"black",
            boxShadow:"none"
        },


    },

    imgLogoSignIn: {
        width: "100%"
    },
    large: {

        backgroundColor: 'white',
        width: theme.spacing(10),
        height: theme.spacing(10),
    }

});


class SignInContainer extends React.Component {
    constructor(props) {
        super(props);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: "",
            password: "",
            error: null
        };
    }

    componentDidMount() {
    localStorage.clear();
    }

    handleNameChange(event) {
        this.setState({name: event.target.value});
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    }

    onSubmit(event) {
        event.preventDefault();
        event.stopPropagation();

        const user = {
            email: this.state.name,
            password: this.state.password
        };

        this.setState({
            error: null
        })

        login(user).then(res => {
            if (res.error) {
                this.setState({
                    error: "Username or password is invalid!"
                })
            }
            if (res.token !== undefined) {
                this.props.history.push('/inputs')
            }
        })
    }

    render() {
        const {classes} = this.props;

        return (<div className="wrapper">
                <div className="page_header">
                    <div className="page page400">
                    <Grid container direction="row" justify="center" alignItems="center">
                        <Grid item xs={12} sm={12} md={12} lg={12} xl={12} >
                            <CardHeader
                                titleTypographyProps={{variant: "h4", color: 'textPrimary'}}
                                title={"Login"}
                            />
                        </Grid>
                    </Grid>
                    </div>
                </div>
                <div className="page page400 pageBody">
                    <div>
                        <h1 className={"title"}>Sign in your account</h1>
                        {this.state.error && <div role="alert">
                            <div className="MuiAlert-message">{this.state.error}</div>
                        </div>}
                        <form className={classes.container} onSubmit={this.onSubmit}>

                            <TextField
                                id="standard-full-width-username"
                                label="Username"
                                placeholder="Enter Username here"
                                autoComplete="off"
                                // helperText="Full width!"
                                fullWidth
                                value={this.state.name}
                                onChange={(event) => this.handleNameChange(event)}
                                margin="normal"
                                type={"email"}
                                InputLabelProps={{
                                    classes: {root: classes.inputRoot},
                                    shrink: true,
                                }}
                               InputProps={{
                                   classes: { input: classes.input}
                               }}

                            />
                            <TextField
                                className="MB30"
                                id="standard-full-width-password"
                                label="Password"
                                placeholder="Enter Password here"
                                fullWidth
                                type="password"
                                autoComplete="current-password"
                                value={this.state.password}
                                onChange={(event) => this.handlePasswordChange(event)}
                                margin="normal"
                                InputLabelProps={{
                                    classes: {root: classes.inputRoot},
                                    shrink: true,
                                }}
                            />

                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                size="large"
                                className={classes.submit}
                            >
                                Sign In
                            </Button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default (withStyles(styles)(SignInContainer));